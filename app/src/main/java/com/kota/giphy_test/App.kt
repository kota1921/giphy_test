package com.kota.giphy_test

import android.app.Application
import androidx.room.Room
import com.facebook.drawee.backends.pipeline.Fresco
import com.kota.giphy_test.data.db.CacheDb
import com.kota.giphy_test.data.remote.Api
import timber.log.Timber

class App : Application() {

    val api by lazy { Api.implementApi(this, Constants.ENDPOINT, Constants.API_KEY) }
    val db by lazy { Room.databaseBuilder(this, CacheDb::class.java, Constants.DB_NAME).build() }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        Fresco.initialize(this)
    }
}
package com.kota.giphy_test

class Constants {
    companion object {
        const val PAGE_SIZE = 50
        const val API_KEY: String = "0xk1qPj5A5GNjEVxuUKMjQs6pglC5OMZ"
        const val ENDPOINT: String = "https://api.giphy.com/v1/"
        const val DB_NAME: String = "cache_db"
    }
}
package com.kota.giphy_test.data.remote.model

import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("url") val gifUrl: String,
    @SerializedName("webp") val webpUrl: String,
    @SerializedName("width") val width: Int,
    @SerializedName("height") val height: Int
)
package com.kota.giphy_test.data.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kota.giphy_test.domain.model.LocalImage
import io.reactivex.Single

@Dao
interface LocalImageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<LocalImage>)

    @Query("SELECT * FROM LocalImage WHERE LocalImage.`query` = :query")
    fun getCachedImages(query: String): DataSource.Factory<Int, LocalImage>

    @Query("SELECT * FROM LocalImage WHERE LocalImage.`query` = :query LIMIT :pageSize")
    fun getTop(pageSize: Int, query: String): Single<List<LocalImage>>

    @Query("DELETE FROM LocalImage WHERE LocalImage.`query` = :query")
    fun clearCache(query: String): Int

    @Query("SELECT COUNT(*) FROM LocalImage WHERE LocalImage.`query` = :query")
    fun getCacheSize(query: String): Single<Int>

}
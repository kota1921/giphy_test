package com.kota.giphy_test.data.remote.model

data class Gif(
    val id: String,
    val url: String,
    val title: String,
    val images: Images,
    val source: String
)
package com.kota.giphy_test.data.remote

import android.content.Context
import com.google.gson.GsonBuilder
import com.kota.giphy_test.Constants.Companion.PAGE_SIZE
import com.kota.giphy_test.data.remote.model.GiphyResponse
import com.readystatesoftware.chuck.ChuckInterceptor
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("gifs/trending")
    fun getTrending(
        @Query("offset") offset: Int = 0,
        @Query("limit") count: Int = PAGE_SIZE
    ): Single<GiphyResponse>

    @GET("gifs/search")
    fun makeSearch(
        @Query("q") query: String,
        @Query("offset") offset: Int = 0,
        @Query("limit") count: Int = PAGE_SIZE
    ): Single<GiphyResponse>

    companion object {
        fun implementApi(
            context: Context,
            endPointProvider: String,
            apiKeyProvider: String
        ): Api {
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor {
                    val originalRequest = it.request()
                    val originalUrl = originalRequest.url()
                    val urlWithKey = originalUrl.newBuilder()
                        .addQueryParameter("api_key", apiKeyProvider)
                        .build()

                    val resultRequest = originalRequest
                        .newBuilder()
                        .url(urlWithKey)
                        .build()
                    it.proceed(resultRequest)
                }
                .addInterceptor(ChuckInterceptor(context))
                .build()

            val retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(endPointProvider)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

            return retrofit.create(Api::class.java)
        }
    }

}
package com.kota.giphy_test.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.kota.giphy_test.domain.model.LocalImage

@Database(
    entities = [LocalImage::class],
    version = 1
)
abstract class CacheDb : RoomDatabase() {
    abstract fun getLocalImageDao(): LocalImageDao
}
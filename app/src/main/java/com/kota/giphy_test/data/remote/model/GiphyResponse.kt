package com.kota.giphy_test.data.remote.model

data class GiphyResponse(
    val data: ArrayList<Gif>,
    val pagination: Pagination
)
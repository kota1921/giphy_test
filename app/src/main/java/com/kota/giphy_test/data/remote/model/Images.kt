package com.kota.giphy_test.data.remote.model

import com.google.gson.annotations.SerializedName

data class Images(
    @SerializedName("fixed_width_downsampled") val fixedWidthDownsampled: Image,
    @SerializedName("fixed_width") val fixedWidth: Image,
    @SerializedName("original") val original: Image
)
package com.kota.giphy_test.presentation.common

import android.graphics.drawable.Animatable
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.imagepipeline.image.ImageInfo
import com.kota.giphy_test.R
import com.kota.giphy_test.domain.model.LocalImage
import com.kota.giphy_test.presentation.base.gone
import com.kota.giphy_test.presentation.base.visible
import kotlinx.android.synthetic.main.item_gif.view.*

class LocalImagePagedAdapter : PagedListAdapter<LocalImage, LocalImagePagedAdapter.ImageVH>(LocalImage.diffUtilCallback) {

    private var onItemClickListener: (LocalImage) -> Unit = {}

    fun setOnItemClickListener(listener: (LocalImage) -> Unit) {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageVH =
        ImageVH(LayoutInflater.from(parent.context).inflate(R.layout.item_gif, parent, false))

    override fun onBindViewHolder(holder: ImageVH, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ImageVH(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(localImage: LocalImage?) {
            with(itemView) {
                viewProgress.visible()
                viewError.gone()

                if (localImage == null) return


                val uri = if (localImage.previewWebpUrl.isNotEmpty()) Uri.parse(localImage.previewWebpUrl)
                else Uri.parse(localImage.previewGifUrl)

                val controlListener = object : BaseControllerListener<ImageInfo>() {
                    override fun onFinalImageSet(id: String?, imageInfo: ImageInfo?, animatable: Animatable?) {
                        super.onFinalImageSet(id, imageInfo, animatable)
                        viewProgress.gone()
                    }

                    override fun onFailure(id: String?, throwable: Throwable?) {
                        super.onFailure(id, throwable)
                        viewError.visible()
                    }
                }

                val controller = Fresco.newDraweeControllerBuilder()
                    .setUri(uri)
                    .setAutoPlayAnimations(true)
                    .setControllerListener(controlListener)
                    .build()

                simpleDraweeView.controller = controller
                simpleDraweeView.aspectRatio = localImage.width / localImage.height.toFloat()

                itemView.setOnClickListener { onItemClickListener(localImage) }
            }
        }

    }
}
package com.kota.giphy_test.presentation.trending

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.kota.giphy_test.App
import com.kota.giphy_test.R
import com.kota.giphy_test.presentation.base.ViewModelFactory
import com.kota.giphy_test.presentation.base.changeVisible
import com.kota.giphy_test.presentation.base.gone
import com.kota.giphy_test.presentation.common.LocalImagePagedAdapter
import com.kota.giphy_test.presentation.host.Route
import com.kota.giphy_test.presentation.single.SingleImageDialog
import kotlinx.android.synthetic.main.fragment_trending.*
import kotlinx.android.synthetic.main.view_error.*

class TrendingFragment : Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this, ViewModelFactory(requireActivity().application as App)).get(TrendingViewModel::class.java)
    }

    private var retrySnackbar: Snackbar? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_trending, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onStop() {
        super.onStop()
        retrySnackbar?.let { if (it.isShown) it.dismiss() }
    }

    private fun init() {
        toolbar.title = getString(R.string.trending_title)
        toolbar.inflateMenu(R.menu.menu_with_search)
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_search -> showSearchFragment()
                else -> return@setOnMenuItemClickListener false
            }
            true
        }

        val adapter = LocalImagePagedAdapter()
        adapter.setOnItemClickListener { SingleImageDialog(it).show(requireFragmentManager()) }
        val layoutManager = StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)

        recyclerViewTrendingList.layoutManager = layoutManager
        recyclerViewTrendingList.adapter = adapter

        viewModel.trendingImages.observe(viewLifecycleOwner, Observer { adapter.submitList(it) })
        viewModel.isTopRefreshing.observe(viewLifecycleOwner, Observer { handleTopLoadingState(it) })
        viewModel.isBottomRefreshing.observe(viewLifecycleOwner, Observer { handleBottomLoadingState(it) })
        viewModel.isTopError.observe(viewLifecycleOwner, Observer { handleTopError(it) })
        viewModel.isBottomError.observe(viewLifecycleOwner, Observer { handleBottomError(it) })

        swipeToRefresh.setOnRefreshListener { viewModel.refresh() }
    }

    private fun showSearchFragment() {
        val activity = requireActivity()
        if (activity is Route) activity.routeToSearch()
    }

    private fun handleTopLoadingState(state: Boolean) {
        swipeToRefresh.isRefreshing = state
        frameLayoutError.gone()
        if (state) {
            progressBarBottomLoading.gone()
        }
    }

    private fun handleBottomLoadingState(state: Boolean) {
        progressBarBottomLoading.changeVisible(state)
    }

    private fun handleTopError(state: Boolean) {
        frameLayoutError.changeVisible(state)
        swipeToRefresh.isRefreshing = false
        progressBarBottomLoading.gone()
    }

    private fun handleBottomError(state: Boolean) {
        if (state) {
            retrySnackbar = Snackbar.make(requireView(), R.string.retry_message, Snackbar.LENGTH_INDEFINITE)
            retrySnackbar?.setAction(R.string.retry_action) { viewModel.retry() }
            retrySnackbar?.show()
        } else retrySnackbar?.let { if (it.isShown) it.dismiss() }
        progressBarBottomLoading.gone()
    }

    companion object {
        fun createFragment() = TrendingFragment()
    }

}
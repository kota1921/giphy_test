package com.kota.giphy_test.presentation.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.kota.giphy_test.App
import com.kota.giphy_test.R
import com.kota.giphy_test.presentation.base.ViewModelFactory
import com.kota.giphy_test.presentation.base.changeVisible
import com.kota.giphy_test.presentation.base.gone
import com.kota.giphy_test.presentation.base.visible
import com.kota.giphy_test.presentation.common.LocalImagePagedAdapter
import com.kota.giphy_test.presentation.host.Route
import com.kota.giphy_test.presentation.single.SingleImageDialog
import com.miguelcatalan.materialsearchview.MaterialSearchView
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.view_error.*

class SearchFragment : Fragment(), MaterialSearchView.SearchViewListener, MaterialSearchView.OnQueryTextListener {

    private val viewModel by lazy {
        ViewModelProviders.of(this, ViewModelFactory(requireActivity().application as App)).get(SearchViewModel::class.java)
    }

    private var retrySnackbar: Snackbar? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_search, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onStop() {
        super.onStop()
        retrySnackbar?.let { if (it.isShown) it.dismiss() }
    }

    private fun init() {
        searchView.showSearch(false)
        searchView.showSearch()
        searchView.setOnQueryTextListener(this)
        searchView.setOnSearchViewListener(this)

        val adapter = LocalImagePagedAdapter()
        adapter.setOnItemClickListener { SingleImageDialog(it).show(requireFragmentManager()) }
        val layoutManager = StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)

        recyclerViewSearchedList.layoutManager = layoutManager
        recyclerViewSearchedList.adapter = adapter

        viewModel.searchedImages.observe(viewLifecycleOwner, Observer { adapter.submitList(it) })
        viewModel.isEmptyRequest.observe(viewLifecycleOwner, Observer { handleEmptyRequestState(it) })

        viewModel.isTopRefreshing.observe(viewLifecycleOwner, Observer<Boolean> { handleTopRefreshingState(it) })
        viewModel.isBottomRefreshing.observe(viewLifecycleOwner, Observer { handleBottomRefreshState(it) })
        viewModel.isResultEmpty.observe(viewLifecycleOwner, Observer { handleEmptyResultState(it) })
        viewModel.isTopError.observe(viewLifecycleOwner, Observer { handleErrorResponse(it) })
        viewModel.isBottomError.observe(viewLifecycleOwner, Observer { if (it) handleBottomError(it) })

        swipeToRefresh.setOnRefreshListener { viewModel.refresh() }
    }

    override fun onSearchViewClosed() {
        val activity = requireActivity()
        if (activity is Route) activity.routeBack()
    }

    override fun onSearchViewShown() {
        viewEmptyRequest.visible()
        swipeToRefresh.isEnabled = false
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        viewModel.onQueryTextSubmit(query ?: "")
        return true
    }

    override fun onQueryTextChange(query: String?): Boolean {
        viewModel.onQueryTextChange(query ?: "")
        return true
    }

    private fun handleTopRefreshingState(state: Boolean) {
        swipeToRefresh.isRefreshing = state
    }

    private fun handleBottomRefreshState(state: Boolean) {
        progressBarBottomLoading.changeVisible(state)
    }

    private fun handleEmptyRequestState(state: Boolean) {
        viewEmptyRequest.changeVisible(state)
        swipeToRefresh.isEnabled = !state
    }

    private fun handleEmptyResultState(state: Boolean) {
        viewEmptySearchResult.changeVisible(state)
        swipeToRefresh.isRefreshing = false
        frameLayoutError.gone()
    }

    private fun handleErrorResponse(state: Boolean) {
        frameLayoutError.changeVisible(state)
        swipeToRefresh.isRefreshing = false
    }

    private fun handleBottomError(state: Boolean) {
        if (state) {
            retrySnackbar = Snackbar.make(requireView(), R.string.retry_message, Snackbar.LENGTH_INDEFINITE)
            retrySnackbar?.setAction(R.string.retry_action) { viewModel.retry() }
            retrySnackbar?.show()
        } else retrySnackbar?.let { if (it.isShown) it.dismiss() }
        progressBarBottomLoading.gone()
    }

    companion object {
        fun createFragment() = SearchFragment()
    }

}
package com.kota.giphy_test.presentation.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun Disposable.attachToComposit(compositeDisposable: CompositeDisposable) {
    compositeDisposable.add(this)
}
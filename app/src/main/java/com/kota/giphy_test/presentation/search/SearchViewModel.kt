package com.kota.giphy_test.presentation.search

import android.text.TextUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.kota.giphy_test.domain.repository.core.SearchImagesRepository
import com.kota.giphy_test.presentation.base.attachToComposit
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit

class SearchViewModel(
    repository: SearchImagesRepository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel() {

    private val querySubject = PublishSubject.create<String>()

    private val query = MutableLiveData<String>()
    private val searchResult = Transformations.map(query) { repository.search(it) }
    val searchedImages = Transformations.switchMap(searchResult) { it.pagedList }

    val isEmptyRequest = MutableLiveData<Boolean>()

    val isResultEmpty: LiveData<Boolean> = Transformations.switchMap(searchResult) { it.operationsState.isTopLoadedEmpty }
    val isTopRefreshing: LiveData<Boolean> = Transformations.switchMap(searchResult) { it.operationsState.isTopRefreshing }
    val isBottomRefreshing: LiveData<Boolean> = Transformations.switchMap(searchResult) { it.operationsState.isBottomRefreshing }
    val isTopError: LiveData<Boolean> = Transformations.switchMap(searchResult) { it.operationsState.isTopError }
    val isBottomError: LiveData<Boolean> = Transformations.switchMap(searchResult) { it.operationsState.isBottomError }

    init {
        querySubject
            .debounce(300, TimeUnit.MILLISECONDS)
            .subscribe(
                { sendQuery(it) },
                { Timber.e(it) }
            )
            .attachToComposit(compositeDisposable)

        isEmptyRequest.postValue(true)
    }

    fun refresh() {
        if (TextUtils.isEmpty(query.value)) return
        searchResult.value?.refresh?.invoke()
    }

    fun onQueryTextChange(query: String) {
        isEmptyRequest.postValue(query.isEmpty())
        querySubject.onNext(query)
    }

    fun onQueryTextSubmit(query: String) {
        if (TextUtils.isEmpty(query)) return
        sendQuery(query)
    }

    private fun sendQuery(query: String) {
        if (query.isEmpty()) return
        this.query.postValue(query)
        searchResult.value?.refresh?.invoke()
    }

    fun retry() = searchResult.value?.retry?.invoke()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
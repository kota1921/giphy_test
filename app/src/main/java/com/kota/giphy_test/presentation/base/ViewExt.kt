package com.kota.giphy_test.presentation.base

import android.view.View

fun View.gone() {
    this.visibility = View.GONE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.changeVisible(isVisible: Boolean?) {
    if (isVisible == true) visible()
    else gone()
}
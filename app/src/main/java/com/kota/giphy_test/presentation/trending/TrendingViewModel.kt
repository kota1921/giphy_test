package com.kota.giphy_test.presentation.trending

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.kota.giphy_test.domain.repository.core.TrendingImagesRepository
import io.reactivex.disposables.CompositeDisposable

class TrendingViewModel(
    repository: TrendingImagesRepository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel() {

    private val trendingImagesResult = MutableLiveData(repository.getImages())
    val trendingImages = Transformations.switchMap(trendingImagesResult) { it.pagedList }

    val isTopRefreshing: LiveData<Boolean> = Transformations.switchMap(trendingImagesResult) { it.operationsState.isTopRefreshing }
    val isBottomRefreshing: LiveData<Boolean> = Transformations.switchMap(trendingImagesResult) { it.operationsState.isBottomRefreshing }
    val isTopError: LiveData<Boolean> = Transformations.switchMap(trendingImagesResult) { it.operationsState.isTopError }
    val isBottomError: LiveData<Boolean> = Transformations.switchMap(trendingImagesResult) { it.operationsState.isBottomError }

    fun refresh() = trendingImagesResult.value?.refresh?.invoke()
    fun retry() = trendingImagesResult.value?.retry?.invoke()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
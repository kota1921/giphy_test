package com.kota.giphy_test.presentation.host

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.kota.giphy_test.R
import com.kota.giphy_test.presentation.search.SearchFragment
import com.kota.giphy_test.presentation.trending.TrendingFragment


class ContainerActivity : AppCompatActivity(), Route {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)
        if (supportFragmentManager.backStackEntryCount == 0) showFragment(TrendingFragment.createFragment())
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount != 1) supportFragmentManager.popBackStack()
        else finish()
    }

    override fun routeBack() = onBackPressed()

    override fun routeToSearch() = showFragment(SearchFragment.createFragment())

    private fun showFragment(fragment: Fragment) {
        val currentDisplayedFragmentTag = supportFragmentManager.findFragmentById(R.id.fragmentContainer)?.tag ?: ""

        if (currentDisplayedFragmentTag == fragment::class.java.simpleName) return

        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, fragment, fragment::class.java.simpleName)
            .addToBackStack(fragment::class.java.simpleName)
            .commit()
    }

}

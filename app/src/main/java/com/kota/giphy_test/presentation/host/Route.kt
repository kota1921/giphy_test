package com.kota.giphy_test.presentation.host

interface Route {
    fun routeBack()
    fun routeToSearch()
}
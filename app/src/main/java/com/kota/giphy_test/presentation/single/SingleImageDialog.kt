package com.kota.giphy_test.presentation.single

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.drawable.Animatable
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentManager
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.imagepipeline.image.ImageInfo
import com.kota.giphy_test.R
import com.kota.giphy_test.domain.model.LocalImage
import com.kota.giphy_test.presentation.base.BaseDialogFragment
import com.kota.giphy_test.presentation.base.gone
import com.kota.giphy_test.presentation.base.visible
import kotlinx.android.synthetic.main.item_gif.view.*

class SingleImageDialog(private val localImage: LocalImage) : BaseDialogFragment() {

    @SuppressLint("InflateParams")
    override fun initDialog(): Dialog {
        val view = LayoutInflater.from(context).inflate(R.layout.item_gif, null, false)
        showImage(view)
        return AlertDialog.Builder(view.context)
            .setView(view)
            .create()
    }

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, fragmentManager.toString())

    private fun showImage(view: View) {
        val uri = if (localImage.originalWebpUrl.isNotEmpty()) Uri.parse(localImage.originalWebpUrl)
        else Uri.parse(localImage.originalGifUrl)

        val controlListener = object : BaseControllerListener<ImageInfo>() {
            override fun onFinalImageSet(id: String?, imageInfo: ImageInfo?, animatable: Animatable?) {
                super.onFinalImageSet(id, imageInfo, animatable)
                view.viewProgress.gone()
            }

            override fun onFailure(id: String?, throwable: Throwable?) {
                super.onFailure(id, throwable)
                view.viewError.visible()
            }
        }

        val controller = Fresco.newDraweeControllerBuilder()
            .setUri(uri)
            .setAutoPlayAnimations(true)
            .setControllerListener(controlListener)
            .build()

        view.simpleDraweeView.controller = controller
        view.simpleDraweeView.aspectRatio = localImage.width / localImage.height.toFloat()
    }
}
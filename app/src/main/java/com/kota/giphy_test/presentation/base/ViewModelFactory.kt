package com.kota.giphy_test.presentation.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kota.giphy_test.App
import com.kota.giphy_test.domain.repository.DataRepository
import com.kota.giphy_test.presentation.search.SearchViewModel
import com.kota.giphy_test.presentation.trending.TrendingViewModel
import io.reactivex.disposables.CompositeDisposable

class ViewModelFactory(private val application: App) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val compositeDisposable = CompositeDisposable()
        if (modelClass.isAssignableFrom(TrendingViewModel::class.java)) {
            return TrendingViewModel(DataRepository(application, compositeDisposable), compositeDisposable) as T
        }
        if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
            return SearchViewModel(DataRepository(application, CompositeDisposable()), compositeDisposable) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}
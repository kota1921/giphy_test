package com.kota.giphy_test.domain.boundary

import androidx.paging.PagedList
import com.kota.giphy_test.data.db.LocalImageDao
import com.kota.giphy_test.data.remote.Api
import com.kota.giphy_test.domain.GifToLocalImage
import com.kota.giphy_test.domain.model.LocalImage
import com.kota.giphy_test.domain.repository.network.OperationsState
import com.kota.giphy_test.presentation.base.attachToComposit
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class LocalImageBoundaryCallback(
    private val api: Api,
    private val localImageDao: LocalImageDao,
    private val pageSize: Int,
    private val query: String,
    private val compositeDisposable: CompositeDisposable,
    private val operationsState: OperationsState
) : PagedList.BoundaryCallback<LocalImage>() {

    private var lastTryLoadedEndItem: LocalImage? = null

    override fun onZeroItemsLoaded() {
        Timber.d("load by query $query")
        val dataSource = if (query.isEmpty()) api.getTrending(count = pageSize)
        else api.makeSearch(query, count = pageSize)

        operationsState.isTopRefreshing.postValue(true)
        dataSource
            .subscribeOn(Schedulers.single())
            .subscribe(
                {
                    cacheImages(GifToLocalImage.convert(it.data, query))
                    operationsState.isTopLoadedEmpty.postValue(it.data.isEmpty())
                },
                { operationsState.isTopError.postValue(true) }
            )
            .attachToComposit(compositeDisposable)
    }

    override fun onItemAtEndLoaded(itemAtEnd: LocalImage) {
        operationsState.isBottomRefreshing.postValue(true)

        lastTryLoadedEndItem = itemAtEnd
        localImageDao.getCacheSize(query)
            .flatMap {
                if (query.isEmpty()) api.getTrending(offset = it, count = pageSize)
                else api.makeSearch(query, offset = it, count = pageSize)
            }
            .subscribeOn(Schedulers.single())
            .subscribe(
                {
                    lastTryLoadedEndItem = null
                    cacheImages(GifToLocalImage.convert(it.data, query))
                    operationsState.isBottomRefreshing.postValue(false)
                },
                {
                    operationsState.isBottomError.postValue(true)
                }
            ).attachToComposit(compositeDisposable)
    }

    fun tryRetryLastLoad(): Boolean {
        if (lastTryLoadedEndItem == null) return false
        onItemAtEndLoaded(lastTryLoadedEndItem!!)
        return true
    }

    private fun cacheImages(images: List<LocalImage>) {
        Single.just(localImageDao)
            .map { it.insert(images) }
            .subscribeOn(Schedulers.single())
            .subscribe(
                { Timber.d("caching done size ${images.size} query $query ") },
                { Timber.e(it) }
            )
            .attachToComposit(compositeDisposable)
    }
}
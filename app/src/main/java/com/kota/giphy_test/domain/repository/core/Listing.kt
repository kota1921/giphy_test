package com.kota.giphy_test.domain.repository.core

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.kota.giphy_test.domain.repository.network.OperationsState

data class Listing<T>(
    val pagedList: LiveData<PagedList<T>>,
    val operationsState: OperationsState,
    val refresh: () -> Unit,
    val retry: () -> Unit
)
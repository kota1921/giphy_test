package com.kota.giphy_test.domain.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.kota.giphy_test.App
import com.kota.giphy_test.Constants.Companion.PAGE_SIZE
import com.kota.giphy_test.domain.GifToLocalImage
import com.kota.giphy_test.domain.boundary.LocalImageBoundaryCallback
import com.kota.giphy_test.domain.model.LocalImage
import com.kota.giphy_test.domain.repository.core.Listing
import com.kota.giphy_test.domain.repository.core.SearchImagesRepository
import com.kota.giphy_test.domain.repository.core.TrendingImagesRepository
import com.kota.giphy_test.domain.repository.network.OperationsState
import com.kota.giphy_test.presentation.base.attachToComposit
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class DataRepository(
    private val application: App,
    private val compositeDisposable: CompositeDisposable
) : TrendingImagesRepository, SearchImagesRepository {

    private val api = application.api
    private val cacheDao = application.db.getLocalImageDao()

    override fun getImages(): Listing<LocalImage> = getImages("")

    override fun search(query: String): Listing<LocalImage> = getImages(query)

    private fun getImages(query: String): Listing<LocalImage> {
        val operationsState = OperationsState()

        val boundaryCallback = LocalImageBoundaryCallback(
            api,
            cacheDao,
            PAGE_SIZE,
            query,
            compositeDisposable,
            operationsState
        )

        val pagedConfig = PagedList.Config.Builder()
            .setPageSize(PAGE_SIZE)
            .build()

        val livePagedList = LivePagedListBuilder(cacheDao.getCachedImages(query), pagedConfig)
            .setBoundaryCallback(boundaryCallback)
            .build()

        return Listing(
            pagedList = livePagedList,
            operationsState = operationsState,
            refresh = { refreshHash(query, operationsState) },
            retry = {
                val retryState = boundaryCallback.tryRetryLastLoad()
                operationsState.isBottomError.postValue(false)
                if (!retryState) refreshHash(query, operationsState)
            }
        )
    }

    private fun refreshHash(query: String, operationsState: OperationsState): LiveData<Any> {
        val dataSource = if (query.isEmpty()) api.getTrending()
        else api.makeSearch(query)

        operationsState.isTopRefreshing.postValue(true)
        Single.zip<List<LocalImage>, List<LocalImage>, Pair<List<LocalImage>, Boolean>>(
            dataSource.map { GifToLocalImage.convert(it.data, query) },
            cacheDao.getTop(PAGE_SIZE, query),
            BiFunction { api, base -> Pair(api, api == base) }
        )
            .filter {
                if (it.second) operationsState.isTopRefreshing.postValue(false)
                query.isNotEmpty() || !it.second
            }
            .map { it.first }
            .doOnSuccess {
                application.db.runInTransaction {
                    cacheDao.clearCache(query)
                    cacheDao.insert(it)
                }
            }
            .subscribeOn(Schedulers.single())
            .subscribe(
                {
                    operationsState.isTopLoadedEmpty.postValue(it.isEmpty())
                    operationsState.isTopRefreshing.postValue(false)
                },
                { operationsState.isTopError.postValue(true) }
            )
            .attachToComposit(compositeDisposable)
        return MutableLiveData()
    }

}
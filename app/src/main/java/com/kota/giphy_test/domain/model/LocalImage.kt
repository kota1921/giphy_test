package com.kota.giphy_test.domain.model

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class LocalImage(
    @PrimaryKey val id: String,
    val previewWebpUrl: String,
    val previewGifUrl: String,
    val originalWebpUrl: String,
    val originalGifUrl: String,
    val title: String,
    val source: String,
    val width: Int,
    val height: Int,
    val query: String = ""
) {

    companion object {
        val diffUtilCallback = object : DiffUtil.ItemCallback<LocalImage>() {
            override fun areItemsTheSame(oldItem: LocalImage, newItem: LocalImage): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: LocalImage, newItem: LocalImage): Boolean =
                oldItem.previewGifUrl == newItem.previewGifUrl && oldItem.previewWebpUrl == newItem.previewWebpUrl
        }
    }

}
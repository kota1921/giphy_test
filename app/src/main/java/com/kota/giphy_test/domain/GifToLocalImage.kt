package com.kota.giphy_test.domain

import com.kota.giphy_test.data.remote.model.Gif
import com.kota.giphy_test.domain.model.LocalImage

class GifToLocalImage {
    companion object {
        fun convert(gifs: List<Gif>, query: String) = gifs.map {
            val fixedWidth = it.images.fixedWidthDownsampled
            val original = it.images.original
            LocalImage(
                it.id,
                fixedWidth.webpUrl,
                fixedWidth.gifUrl,
                original.webpUrl,
                original.gifUrl,
                it.title,
                it.source,
                fixedWidth.width,
                fixedWidth.height,
                query
            )
        }

    }
}
package com.kota.giphy_test.domain.repository.core

import com.kota.giphy_test.domain.model.LocalImage

interface TrendingImagesRepository {
    fun getImages(): Listing<LocalImage>
}
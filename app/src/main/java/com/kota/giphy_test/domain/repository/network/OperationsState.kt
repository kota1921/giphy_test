package com.kota.giphy_test.domain.repository.network

import androidx.lifecycle.MutableLiveData

data class OperationsState(
    val isTopRefreshing: MutableLiveData<Boolean> = MutableLiveData(),
    val isBottomRefreshing: MutableLiveData<Boolean> = MutableLiveData(),
    val isTopLoadedEmpty: MutableLiveData<Boolean> = MutableLiveData(),
    val isTopError: MutableLiveData<Boolean> = MutableLiveData(),
    val isBottomError: MutableLiveData<Boolean> = MutableLiveData()
)
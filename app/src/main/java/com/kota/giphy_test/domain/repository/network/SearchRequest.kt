package com.kota.giphy_test.domain.repository.network

data class SearchRequest(
    val searchRequest: String = "",
    val startPosition: Int = 0
)